#!/usr/bin/env bash
#
# /home/admin/deploy/deploy.sh
#
# This bash script is used to deploy new ODE war files and processes.
# Copy new war files and Feature4Process folder to /home/admin/deploy using admin credentials
#
# This script will stop tomcat, change owner, move the files to the proper directory and restart tomcat
sudo service tomcat7 stop
sudo chown tomcat7: ~/*.war
sudo chown -R tomcat7: Feature4Process
sudo mv ~/*.war /var/lib/tomcat7/webapps/
sudo mv Feature4Proces /var/lib/tomcat7/webapps/ode/WEB-INF/Processes/Feature4Process
tree -L 2 /var/lib/tomcat7/webapps
tree -L 2 /var/lib/tomcat7/webapps/ode/WEB-INF/processes/
sudo service tomcat7 start
