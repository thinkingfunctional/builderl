-module(jena_installer).
-export([runner/3, version/0]).

-include("include/global.hrl").
-define(VERSION, "Jena Installer " ++ ?TAG).

version() ->
    ?VERSION.

run_step(Where, Step, Config ) ->
		core_installer:runner(Where, [Step], Config).

runner(Where, Steps, Config) ->
    RunStep = fun(Step) -> run_step(Where, Step, Config) end,
    lists:map(RunStep, Steps).
