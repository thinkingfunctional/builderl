-module(ode_installer).
-export([runner/3]).

run_step(_Where, [{sleep, Time}], _Config) ->
    io:format("Sleeping for ~s seconds.~n", [integer_to_list(Time)]),
    timer:sleep(Time * 1000);

run_step(_Where, [{sleep, Time}, {reason, Reason}], _Config) ->
    io:format("Sleeping for ~s seconds : ~s.~n", [integer_to_list(Time), Reason]),
    timer:sleep(Time * 1000);

run_step(Where, Step, Config) ->
		core_installer:runner(Where, Step, Config).

runner(Where, Steps, Config) ->
    RunStep = fun(Step) -> run_step(Where, [Step], Config) end,
    lists:map(RunStep, Steps).
