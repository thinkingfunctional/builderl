-module(core_installer).
-export([runner/3, version/0]).

-include("include/global.hrl").
-define(VERSION, "Core Installer " ++ ?TAG).

version() ->
    ?VERSION.

echo(Cmd) ->
    io:format("Executing now: ~s~n", [Cmd]).

result(Where, Step) ->
    Cmd = [Step,"\n"],
    echo(Cmd),
    ec2ssh:ssh(Where, Cmd).

result(Where, Step, Timeout) ->
    Cmd = [Step,"\n"],
    echo("Long Run: " ++ Cmd),
    ec2ssh:ssh(Where, Cmd, Timeout).

run_step(Where, [{external, M}, {function, F}], Config) when is_atom(M) == true ; is_atom(F) == true ->
		%% [{create_database, ""}, {}]
		M:runner(Where, [F], Config);

run_step(Where, [{long_run_script, Script}], _Config) ->
    Cmd = io_lib:format("/usr/bin/nohup < /dev/null > /dev/null ~s &", [Script]),
    result(Where, Cmd, (60 * 1000) * 60);

run_step(Where, [{install, Packages}], Config) ->
    OSInstaller = proplists:get_value(os_install_cmd, Config, "apt-get"),
    InstallerArgs = proplists:get_value(installer_args, Config, "--yes"),
    Cmd = io_lib:format("sudo ~s install ~s ~s", [OSInstaller, InstallerArgs, Packages]),
    result(Where, Cmd);

run_step(Where, [{install, Packages}, {timeout, Timeout}], Config) ->
    OSInstaller = proplists:get_value(os_install_cmd, Config, "apt-get"),
    InstallerArgs = proplists:get_value(installer_args, Config, "--yes"),
    Cmd = io_lib:format("sudo ~s install ~s ~s", [OSInstaller, InstallerArgs, Packages]),
    result(Where, Cmd, Timeout);

run_step(Where, [{service, Service}, {action, Action}], _Config) when Action == start ; Action == stop ; Action == restart ->
    Cmd = io_lib:format("sudo service ~s ~s", [Service, atom_to_list(Action)]),
    result(Where, Cmd);
run_step(Where, [{mysql, Action}], _Config) when Action == start ; Action == stop ; Action == restart ->
    Cmd = io_lib:format("sudo service mysql ~s", [Action]),
    result(Where, Cmd);
run_step(_Where, [{mysql, UnsupportedAction}], _Config) ->
    io:format("Unsupported action '~s'.~nNot executed.~n", [UnsupportedAction]);

run_step(Where, [{cmd, Command}], _Config) ->
    %% Simple OS command to be run
    result(Where, Command);
run_step(Where, [{cmd, Command}, {args, Args}], _Config) ->
    %% Simple OS command with Arguments to the shell command
    Cmd = io_lib:format("~s ~s", [Command, Args]),
    result(Where, Cmd);
run_step(Where, [{cmd, Command}, {timeout, Timeout}], _Config) when is_integer(Timeout) ->
    io:format("Running cmd '~s' with a timeout of ~p seconds.~n", [Command, Timeout / 1000]),
    Cmd = io_lib:format("~s", [Command]),
    result(Where, Cmd, Timeout);
run_step(Where, [{cmd, Command}, {timeout, Timeout}], _Config) when is_atom(Timeout) ->
    io:format("Running cmd '~s' with a timeout of ~p.~n", [Command, Timeout]),
    Cmd = io_lib:format("~s", [Command]),
    result(Where, Cmd, Timeout);


run_step(Where, [{file_touch, Filename}], _Config) ->
    Cmd = io_lib:format("touch ~s", [Filename]),
    result(Where, Cmd);
run_step(Where, [{file_touch, Filename}, {sudo, true}], _Config) ->
    Cmd = io_lib:format("sudo touch ~s", [Filename]),
    result(Where, Cmd);

run_step(Where, [{file_append, Filename}, {phrase, Phrase}], _Config) ->
    Cmd = io_lib:format("sudo cat >> ~s <<< '~s'", [Filename, Phrase]),
    result(Where, Cmd);

run_step(Where, [{replace, Filename}, {search, Search}, {replace, Replace}], _Config) ->
		Cmd = io_lib:format("sudo perl -pi -e 's/~s/~s/g' ~s", [Search, Replace, Filename]),
		result(Where, Cmd);

run_step(Where, [{debconf_set, Settings}], _Conf ) ->
    Cmd = io_lib:format("sudo echo ~s | sudo /usr/bin/debconf-set-selections", [Settings]),
    result(Where, Cmd);

run_step(Where, [{apt, Package}], _Config) ->
    %% Install OS Package
    Cmd = io_lib:format("dpkg -i ~s", [Package]),
    result(Where, Cmd);
run_step(Where, [{apt, Package}, remove], _Config) ->
    %% Remove an OS Package
    Cmd = io_lib:format("dpkg -u ~s", [Package]),
    result(Where, Cmd);

run_step(Where, [{curl, Url}], _Config) ->
    Cmd = io_lib:format("sudo wget ~s", [Url]),
    result(Where, Cmd);
run_step(Where, [{curl, Url}, {to, DestDir}], _Config) ->
    Cmd = io_lib:format("sudo wget ~s -O ~s", [Url, DestDir]),
    result(Where, Cmd);

run_step(Where, [{cp, From}, {to, To}], _Config) ->
    %% Copies file 'From' to 'To'
    Cmd = io_lib:format("sudo cp ~s ~s", [From, To]),
    result(Where, Cmd);

run_step(Where, [{mv, From}, {to, To}], _Config) ->
    %% Moves a file on the remote server from 'From' to 'To'
    Cmd = io_lib:format("sudo mv ~s ~s", [From, To]),
    result(Where, Cmd);

run_step(Where, [{rm, Filename}], _Config) ->
    %% Removes a file on the remote server
    Cmd = io_lib:format("sudo rm -f ~s", [Filename]),
    result(Where, Cmd);

run_step(Where, [{upload, Filename}, {to, Directory}], _Config) ->
    Cmd = [{filename, Filename}, {to, Directory}],
    io:format("Uploading ~s to ~s.~n", [Filename, Directory]),
    ec2ssh:scp(Where, Cmd);

run_step({Hostname, Username, Password}, [{fetch, Filename}, {to, Destination}], _Config) ->
    %% Downloads a file from the same remote host
    %% "scp Username:Password@Hostname:Filename Destination"
    Cmd = io_lib:format("scp ~s:~s@~s:~s ~s", [Username, Password, Hostname, Filename, Destination]),
    echo(Cmd),
    os:cmd(Cmd);

run_step(Where, [{mkdir, Directory}], _Config) ->
    %% make directory on server
    Cmd = io_lib:format("sudo mkdir -p ~s", [Directory]),
    result(Where, Cmd);

run_step(Where, [{chmod, Filename}, {permissions, Permissions}], _Config) ->
    %% Change permissions on a filesystem object
    Cmd = io_lib:format("sudo chmod ~s ~s", [Permissions, Filename]),
    result(Where, Cmd);

run_step(Where, [{chown, Filename}, {owner, Owner}], _Config) ->
    %% Change ownership of a filesystem object
    Cmd = io_lib:format("sudo chown ~s ~s", [Owner, Filename]),
    result(Where, Cmd);

run_step(Where, [{unzip, Filename}], _Config) ->
    %% Unzips a file in place
    Cmd = io_lib:format("sudo unzip -o ~s", [Filename]),
    result(Where, Cmd);
run_step(Where, [{unzip, Filename}, {to, Destination}], _Config) ->
    %% Unzips a file in place
    Cmd = io_lib:format("sudo unzip -o ~s -d ~s", [Filename, Destination]),
    result(Where, Cmd);

run_step(Where, [{service, Service}, {state, State}], _Config) ->
    %% Starts or stops a service
    Cmd = io_lib:format("sudo service ~s ~s", [Service, State]),
    result(Where, Cmd);
run_step(Where, [{service, Service}, {status, Enabled}], _Config) ->
    %% update-rc.d [-n] name disable|enable [ S|2|3|4|5 ]
    %% Starts or stops a service
    Cmd = io_lib:format("sudo update-rc.d -n ~s ~s", [Service, Enabled]),
    result(Where, Cmd);
run_step(Where, [{service, Service}, {enabled, Enabled}, {runlevel, RunLevel}], _Config) ->
    %% Starts or stops a service
    %% update-rc.d [-n] name disable|enable [ S|2|3|4|5 ]
    Cmd = io_lib:format("sudo update-rc.d -n ~s ~s ~s", [Service, Enabled, RunLevel]),
    result(Where, Cmd).

runner(Where, Steps, Config) ->
    RunStep = fun(Step) -> run_step(Where, Step, Config) end,
    lists:map(RunStep, Steps).
