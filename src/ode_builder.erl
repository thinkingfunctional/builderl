-module(ode_builder).
-export([
         version/0,
         install/1
        ]).

-include("include/global.hrl").
-define(VERSION, "Apache ODE Builder " ++ ?TAG).
-define(CONFIG, "config/ode_config.cfg").
version() ->
    ?VERSION.

install({private_key, PrivateConfig}) ->
    {ok, [Config]} = file:consult(?CONFIG),
    io:format("~s:install({private_key, PrivateConfig)~nPrivateConfig: ~p~nConfig: ~p~n", [?MODULE, PrivateConfig, Config]),
    case proplists:get_value(steps_file, Config) of
        [] ->
            ode_installer:runner(PrivateConfig, steps(), Config);
        StepsFile ->
            io:format("Reading file '~s' for installation instructions~n", [StepsFile]),
            ode_installer:runner(PrivateConfig, steps(StepsFile), Config)
    end;
install(Target) ->
    Comment = proplists:get_value(comment, Target,
                                  "Running against " ++ proplists:get_value(hostname, Target)),
    Where = [{hostname, proplists:get_value(hostname, Target)},
             {username, proplists:get_value(username, Target)},
             {password, proplists:get_value(password, Target)}],
    io:format("ODE Builder: ~p~n", [Comment]),
    ode_installer:runner(Where, steps()).

steps(Filename) ->
    {ok, [Steps]} = file:consult(Filename),
    proplists:get_value(steps, Steps, []).

steps() ->
    [
     %% [{cmd, "ls"}, {args, "-l"}],
     %% [{cmd, "pwd"}],
     %% [{apt, "tomcat7"}],
     %% [{apt, "unzip"}],
     %% [{curl, "http://apache.mirrors.tds.net/ode/apache-ode-war-1.3.6.zip"}, {destination, "./home/vagrant"}],
     %% [{cp, "./home/vagrant/apache-ode-war-1.3.6.zip"}, {to, "/tmp"}],
     %% [{mv, "/tmp/apache-ode-war-1.3.6.zip"}, {to, "/tmp/ode.zip"}],
     %% [{unzip, "/tmp/ode.zip"}, {to, "/tmp" }],
     %% [{cmd, "ls"}, {args, "-l /tmp"}],
     %% [{unzip, "/tmp/ode.zip"}]
     %% [{service, "mysqld"}, {enabled, "yes"}]
     %% [{fetch, "/etc/hosts"}, {to, "/tmp/"}]
     [{mkdir, "/tmp/test/maia"}],
     [{chmod, "/tmp/test/maia"}, {permissions, "go-r"}]
    ].
