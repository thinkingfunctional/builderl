-module(server_builder).
-export([
         version/0,
         init_amazon/0,
         start_server/0,
         start_server/1,
         terminate_all/0,
         list_all/0,
         list_all/1,
         list_all/2,
         instance_dns_name/1,
         terminate/1,
         %% THIS IS ONLY PUBLISHED TO MAKE SPAWN/3 WORK!
         monitor_instances/2,
         wait_for_instance/5
        ]).
-include("include/global.hrl").
-define(VERSION, "Server Builder " ++ ?TAG).
-define(RUNNING, 16).
-define(ONESEC, 1000).
-define(MAXRETRIES, 60).
-define(INITIALTRIES, 0).
-include("deps/erlcloud/include/erlcloud.hrl").
-include("deps/erlcloud/include/erlcloud_ec2.hrl").
-include("deps/erlcloud/include/erlcloud_aws.hrl").

%% Sequence of events:
%% 1) init_amazon
%% 2) start_server with a default configuration
%%    or
%%    start_server with a custom configuration
%% 3) After a successful launch of the server(s), start monitoring
%% 4) Should any of the instance don't start then we have to fail
%%    the script

version() ->
    ?VERSION.

init_amazon() ->
    ssl:start(),
    erlcloud:start(),
    [{username, _Username},{access_key_id, AccessKeyId}, {secret_access_key, SecretAccessKey}] = config:amazon(kai),
    erlcloud_ec2:configure(AccessKeyId, SecretAccessKey).

start_server() ->
    init_amazon(),
    start_server("kai").

start_server(FromConfig) ->
    {ok, Config} = file:consult("servers/" ++ FromConfig ++ "_server.cfg"),
    Configuration = proplists:get_value(configuration, Config),
    Credentials = proplists:get_value(credentials, Config),
    io:format("From  : '~s'~n"
              "Config: '~p'~n"
              "Creds : '~p'~n", [FromConfig, Configuration, Credentials]),
    start_server(Configuration, Credentials, []).

config2record(Configuration) ->
    #ec2_instance_spec{
       image_id = proplists:get_value(image_id, Configuration, ""),
       key_name = proplists:get_value(key_name, Configuration, ""),
       instance_type = proplists:get_value(instance_type, Configuration, ""),
       availability_zone = proplists:get_value(availability_zone, Configuration, "us-east-1a"),
       min_count = proplists:get_value(min_count, Configuration, 1),
       max_count = proplists:get_value(mx_count, Configuration, 1)}.

start_server(Configuration, Credentials, _ShellCommand) ->
    case erlcloud_ec2:run_instances(config2record(Configuration)) of
        {error, {ErrorType , ErrorCode , ErrorShort , ErrorLong}} ->
            io:format("ErrorType  : ~s~n"
                      "ErrorCode  : ~p~n"
                      "ErrorShort : ~s~n"
                      "ErrorLong  : ~p~n", [ErrorType, ErrorCode, ErrorShort, ErrorLong]),
            erlang:halt(einval);
        {ok, [{reservation_id, _ReservationId}, {owner_id, _OwnerId}, {instances_set, Instances}]} ->
            %% The server(s) started ... now let's monitor their progress
            monitor_instances(Instances, Credentials);
        AnythingElse ->
            %% Here we encountered just another error, bail out
            io:format("ERROR: '~p'~n", [AnythingElse]),
            AnythingElse
    end.

terminate([]) ->
    io:format("Nothing to shut down.~n");
terminate(List) ->
    erlcloud_ec2:terminate_instances(List).

terminate_all() ->
    terminate(list_all()).

list_instances([], _Status, Acc) ->
    Acc;
list_instances([Instance | InstancesList], Status, Acc) ->
    [{_, CurrentStatus}, _] = proplists:get_value(instance_state, Instance),
    case Status == CurrentStatus of
        true ->
            InstanceName = proplists:get_value(instance_id, Instance),
            list_instances(InstancesList, Status, [InstanceName | Acc]);
        false ->
            list_instances(InstancesList, Status, Acc)
    end.

%% This lists all running instances without arguments
list_all() ->
    {ok, InstancesList} = erlcloud_ec2:describe_instances(),
    list_all(InstancesList, []).

%% This lists all running instances of a given list in the
%% EC2 environment
list_all(List) ->
    list_all(List, []).

%% There are multiple reservations per list possible
%% and each of the reservations has its own lists of instances
list_all([], Acc) ->
    lists:merge(Acc);
list_all([Reservation | ReservationsList], Acc) ->
    InstancesSet = proplists:get_value(instances_set, Reservation),
    Instances = list_instances(InstancesSet, ?RUNNING, []),
    list_all(ReservationsList, [Instances|Acc]).


%% Finds a specific IP address by an instance name
%% 1) Get all instances
%% 2) Find named instance
%% 3) Extract public IP address
%% 4) return said IP address
instance_dns_name(InstanceName) ->
    {ok, InstancesList} = erlcloud_ec2:describe_instances(),
    find_instance(InstancesList, InstanceName, []).

find_instance([], _InstanceName, []) ->
    [];
find_instance([], _InstanceName, Acc) ->
    lists:flatten(Acc);
find_instance([Head|Tail], InstanceName, Acc) ->
    InstancesSet = proplists:get_value(instances_set, Head),
    Instances = find_instance_by_name(InstancesSet, InstanceName, []),
    case Instances of
        [] ->
            find_instance(Tail, InstanceName, Acc);
        Instance ->
            find_instance(Tail, InstanceName, [Instance|Acc])
    end.

find_instance_by_name([], _InstanceName, Acc) ->
    Acc;
find_instance_by_name([Instance | InstancesList], InstanceName, Acc) ->
    CurrentInstanceName = proplists:get_value(instance_id, Instance),
    case InstanceName == CurrentInstanceName of
        true ->
            InstanceDNSName = proplists:get_value(dns_name, Instance),
            find_instance_by_name(InstancesList, InstanceName, [InstanceDNSName | Acc]);
        false ->
            find_instance_by_name(InstancesList, InstanceName, Acc)
    end.

%% This function will cycle through all the instances and find instances that are
%% When there is only ONE instance started, then the tuple looks like this:
%% monitor_instances(Instances, [{module, BuilderModule}]),

monitor_instances([], _Credentials) ->
    ok;
monitor_instances([Instance | Rest], Credentials) ->
    InstanceName = proplists:get_value(instance_id, Instance),
    [{code, _Code}, {name, State}] = proplists:get_value(instance_state, Instance),
    case State of
        "pending" ->
            spawn(?MODULE, wait_for_instance, [InstanceName, ?ONESEC, ?MAXRETRIES, ?INITIALTRIES, Credentials]);
        AnythingElse ->
            %% As we spun up EC2 instances and they are NOT in "pending" mode
            %% We terminate them as it is an undefined state
            io:format("ERROR: ~p~n", [AnythingElse]),
            terminate_all(),
            erlang:halt(eproto)
    end,
    monitor_instances(Rest, Credentials).

%% This function will monitor an instance and see if it comes online in about 10 sec
%% If it fails ... we shall see
wait_for_instance(Instance, TimeOut, MaxTries, Try, Credentials) when Try < MaxTries ->
    init_amazon(),
    %% io:format("Monitoring for ~s Pid: (~p)~n", [Instance, self()]),
    case lists:member(Instance, server_builder:list_all()) of
        true ->
            %% Credentials holds the module, let's start the installer for this instance
						Nickname = proplists:get_value(nickname, Credentials),
            Module = proplists:get_value(module, Credentials),
            DNSName = instance_dns_name(Instance),
            io:format("Instance '~s' is ready. Starting '~p' on ~s~n", [Instance, Module, DNSName]),
            %% Run the installer module
	    StartTime = calendar:local_time(),
            Result = Module:install({private_key, [{hostname, DNSName}] ++ proplists:delete(hostname, Credentials)}),
	    EndTime = calendar:local_time(),
	    took(Nickname, DNSName, StartTime, EndTime),
	    Result;
        false ->
            timer:sleep(TimeOut),
            io:format("Retry #~b of Instance ~s (~p)~n", [Try, Instance, self()]),
            server_builder:wait_for_instance(Instance, TimeOut, MaxTries, Try + 1, Credentials)
    end;
wait_for_instance(Instance, TimeOut, MaxTries, _Try, _Credentials) ->
    %% One or more instances failed to startup on time ... let's kill all since we can't access anything
    %% if it did not start up
    io:format("Instance '~s' (~p) failed to come online after ~b seconds. Giving up.~n", [Instance, self(), TimeOut * MaxTries]),
    terminate_all(),
    erlang:halt(etimedout).

took(Nickname, Host, StartTime, EndTime) ->
		{ok, File} = file:open("nicknames", [append]),
		file:write(File, ["{", "\"Nickname\"", ":", "\"", Nickname, "\"", ", ", "\"Dns\"", ":", "\"", Host, "\"",  "}", "\n" ] ),
		file:close(File),
		io:format(lists:flatten(elapsed(Host, calendar:time_difference(StartTime, EndTime)))).

elapsed(Host, {Days, {Hours, Minutes, Seconds}}) when Days > 0 ->
		io_lib:format("Installation of '~s' took ~b days, ~b hours, ~b minutes and ~b seconds~n", [Host, Days, Hours, Minutes, Seconds]);
elapsed(Host, {_Days, {Hours, Minutes, Seconds}}) when Hours > 0 ->
		io_lib:format("Installation of '~s' took ~b hours, ~b minutes and ~b seconds~n", [Host, Hours, Minutes, Seconds]);
elapsed(Host, {_Days, {_Hours, Minutes, Seconds}}) ->
		io_lib:format("Installation of '~s' took ~b minutes and ~b seconds~n", [Host, Minutes, Seconds]).
