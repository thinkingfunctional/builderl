-module(ec2ssh).
-export([
         ssh/2,
         ssh/3,
         scp/2
        ]).

-define(TIMEOUT, infinity).
-define(WINDOWSIZE, 1024 * 1024 * 2). % 2MB Buffer .. some stuff is VERY big on output (about 16,000 lines of messages)
-define(MAXPACKETSIZE, 1024).

%% [{hostname, Hostname}, {username, Username}, {nickname, _Nickname}, {module, _Module}] = Options

ssh(Options, Cmd) ->
    Hostname = proplists:get_value(hostname, Options, "localhost"),
    Username = proplists:get_value(username, Options, "root"),
    establish_ssh(Hostname, 180, 0),
    ssh:start(),
    ConnectOptions = [{user, Username},
                      {user_dir, ".ssh"},
                      {key_cb, private_key},
                      {user_interaction, false},
                      {connect_timeout, ?TIMEOUT},
                      {silently_accept_hosts, true}],
    case ssh:connect(Hostname, 22, ConnectOptions, ?TIMEOUT) of
        {ok, ConnRef} ->
            case ssh_connection:session_channel(ConnRef, ?WINDOWSIZE, ?MAXPACKETSIZE, ?TIMEOUT) of
                {ok, ChannId} ->
                    ssh_connection:ptty_alloc(ConnRef, ChannId, [], ?TIMEOUT),
                    ssh_connection:exec(ConnRef, ChannId, Cmd, ?TIMEOUT),
                    collect_data();
                {error, Reason} ->
                    io:format("SSH CONNECT ERROR: ~p~n", [Reason])
            end;
        {error, Reason} ->
            error_logger:error_msg("SSH Connection Error: ~p~n", [Reason])
    end,
    ssh:stop(),
    ok.

ssh(Options, Cmd, Timeout) ->
    Hostname = proplists:get_value(hostname, Options, "localhost"),
    Username = proplists:get_value(username, Options, "root"),
    establish_ssh(Hostname, 180, 0),
    ssh:start(),
    TimeOut = Timeout * 1000,
    ConnectOptions = [{user, Username},
                      {user_dir, ".ssh"},
                      {key_cb, private_key},
                      {user_interaction, false},
                      {connect_timeout, TimeOut},
                      {silently_accept_hosts, true}],
    case ssh:connect(Hostname, 22, ConnectOptions, TimeOut) of
        {ok, ConnRef} ->
            case ssh_connection:session_channel(ConnRef, ?WINDOWSIZE, ?MAXPACKETSIZE, TimeOut) of
                {ok, ChannId} ->
                    ssh_connection:ptty_alloc(ConnRef, ChannId, [], TimeOut),
                    ssh_connection:exec(ConnRef, ChannId, Cmd, TimeOut),
                    collect_data();
                {error, Reason} ->
                    io:format("SSH CONNECT ERROR: ~p~n", [Reason])
            end;
        {error, Reason} ->
            error_logger:error_msg("SSH w/Timeout Connection Error: '~p'~n", [Reason])
    end,
    ssh:stop(),
    ok.

scp(Options, Cmd) ->
    Hostname = proplists:get_value(hostname, Options, "localhost"),
    Username = proplists:get_value(username, Options, "root"),
    establish_ssh(Hostname, 180, 0),
    ssh:start(),
    [{filename, Filename}, {to, Destination}] = Cmd,
    ConnectOptions = [{user, Username},
                      {user_dir, ".ssh"},
                      {key_cb, private_key},
                      {user_interaction, false},
                      {silently_accept_hosts, true}],
    case ssh:connect(Hostname, 22, ConnectOptions) of
        {ok, SshConnection} ->
            {ok, ChannelPid} = ssh_sftp:start_channel(SshConnection),
            {ok, Data} = file:read_file(Filename),
            ssh_sftp:write_file(ChannelPid, Destination, Data, 10000 );
        {error, Reason} ->
            io:format("~n**** TIMED OUT ****~n~n"),
            Reason
    end,
    ssh:stop().

collect_data() ->
    collect_data([]).
collect_data(Data) ->
    receive
        {ssh_cm, _, {data, _, _, Datum}} ->
            %% JUST print current line
            io:format("~s", [Datum]),
            collect_data(Data);

        {ssh_cm, _, {closed, _Information}} ->
            lists:reverse(Data);

        {ssh_cm, _} ->
            collect_data(Data);

        {ssh_cm,_Pid, {exit_status, _ChannelId, _ExitStatus}} ->
            %% Ignore exit statuses
            %% io:format("Channel ~p exited with Status ~p~n", [ChannelId, ExitStatus]),
            collect_data(Data);

        _Msg ->
            %% Unhandled messages are being ignored
            collect_data(Data)
    end.

establish_ssh(Hostname, MaxTries, Try) when Try =< MaxTries ->
    case gen_tcp:connect(Hostname, 22, [{active, once}], 1000) of
        {ok, S} ->
            %% YAY! It works!
            gen_tcp:close(S);
        {error, timeout} ->
            %% Timeout occurred
            error_logger:info_msg("Attempt ~b to connect to ~s", [Try, Hostname]),
            timer:sleep(1000),
            establish_ssh(Hostname, MaxTries, Try + 1);
        {error, econnrefused} ->
            %% Assuming that SSH has just come online but still takes a few secs to be responsive
            %% Let's start over
            error_logger:info_msg("Attempt ~b to connect to ~s", [Try, Hostname]),
            timer:sleep(1000),
            establish_ssh(Hostname, MaxTries, Try + 1);
        Anything ->
            error_logger:error_msg("establish_ssh: ~p~n", [Anything]),
            establish_ssh(Hostname, MaxTries, Try + 1)
    end;
establish_ssh(Hostname, MaxTries, _Try) ->
    error_logger:error_msg("Host ~s did not open SSH on time, giving up after ~b tries.", [Hostname, MaxTries]).
