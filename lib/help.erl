-module(help).
-export([
         version/0,
         usage/1,
         usage/2
        ]).

-include("include/global.hrl").
-define(VERSION, "Help Version " ++ ?TAG).

version() ->
    ?VERSION.

usage(no_args) ->
    io:format("ERROR: You did not provide any args.~n"),
    halt(1).

usage(no_such_module, Module) ->
    io:format("ERROR: No such module '~s'.~n", [Module]),
    halt(2).
