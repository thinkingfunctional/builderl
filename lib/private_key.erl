-module(private_key).
-export([user_key/2, is_host_key/4, version/0]).

-include_lib("public_key/include/public_key.hrl").

-include("include/global.hrl").
-define(VERSION, "Private Key " ++ ?TAG).
-define(PRIVATE_KEY, ".ssh/builderl.pem").


version() ->
    ?VERSION.

is_host_key(_,_,_,_) ->
    %% We ASSUME here, that all is kosher
    %% TODO: fix assumption
    true.

user_key(_Algorithm, _Options) ->
    case file:read_file(?PRIVATE_KEY) of
        {ok, PKey} ->
            %% decode pem file
            [RSAEntry] = public_key:pem_decode(PKey),
            %% Extract key
            Key = public_key:pem_entry_decode(RSAEntry, ""),
            %% return key
            {ok, Key};
        {error, Reason} ->
            %% BOOM!
            Reason
    end.
