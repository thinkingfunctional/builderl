-module(posix).
-export([codes/0, all_codes/0, version/0]).

-include("include/global.hrl").
-define(VERSION, "POSIX Compliant Error Messages " ++ ?TAG).

version() ->
    ?VERSION.

codes() ->
    Codes = [eacces
            , eagain
            , ebadf
            , ebusy
            , edquot
            , eexist
            , efault
            , efbig
            , eintr
            , einval
            , eio
            , eisdir
            , eloop
            , emfile
            , emlink
            , enametoolong
            , enfile
            , enodev
            , enoent
            , enomem
            , enospc
            , enotblk
            , enotdir
            , enotsup
            , enxio
            , eperm
            , epipe
            , erofs
            , espipe
            , esrch
            , estale
            , exdev],
    [io:format("~s: ~s~n", [X, file:format_error(X)]) || X  <- Codes].

all_codes() ->
    Codes = [
             e2big,
             eacces,
             eaddrinuse,
             eaddrnotavail,
             eafnosupport,
             eagain,
             ealready,
             ebadf,
             ebadmsg,
             ebusy,
             ecanceled,
             echild,
             econnaborted,
             econnrefused,
             econnreset,
             edeadlk,
             edestaddrreq,
             edom,
             edquot,
             eexist,
             efault,
             efbig,
             ehostunreach,
             eidrm,
             eilseq,
             einprogress,
             eintr,
             einval,
             eio,
             eisconn,
             eisdir,
             eloop,
             emfile,
             emlink,
             emsgsize,
             emultihop,
             enametoolong,
             enetdown,
             enetreset,
             enetunreach,
             enfile,
             enobufs,
             enodata,
             enodev,
             enoent,
             enoexec,
             enolck,
             enolink,
             enomem,
             enomsg,
             enoprotoopt,
             enospc,
             enosr,
             enostr,
             enosys,
             enotconn,
             enotdir,
             enotempty,
             enotrecoverable,
             enotsock,
             enotsup,
             enotty,
             enxio,
             eopnotsupp,
             eoverflow,
             eownerdead,
             eperm,
             epipe,
             eprotonosupport,
             eprototype,
             eproto,
             erange,
             erofs,
             espipe,
             esrch,
             estale,
             etimedout,
             etime,
             etxtbsy,
             ewouldblock,
             exdev
            ],
    [io:format("~-20s: ~50s~n", [X, file:format_error(X)]) || X  <- Codes].

