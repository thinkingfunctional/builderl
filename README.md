BuildErl
--------

Build Control Tool for CI

This is a modular builder control tool for Ansible and friends.

Modules
--------
As it stands right now, there are a few mandatory functions in each module:

* version/0 -> returns the version of the module
* install/1 -> installs the module's project on Target
* uninstall/1 -> removes the module's project from Target

### Modules need to be compiled to be used like this: ###

    erl -make

This will compile the needed files in the right places.

Dependencies
------------
Dependencies are handled by rebar.
Run

    ./rebar get-deps compile

and all dependencies are being downloaded and compiled.

Amazon
------
There are two ways of providing log in information.

A)

    export AWS_ACCESS_KEY_ID=<Your AWS Access Key>
    export AWS_SECRET_ACCESS_KEY=<Your AWS Secret Access Key>

B)

Editing and compiling the config.erl file and update the amazon configuration

Usage
-----

    erl -pa ebin deps/*/ebin
    
    Erlang/OTP 17 [erts-6.4] [source] [64-bit] [smp:4:4] [async-threads:10] [hipe] [kernel-poll:false] [dtrace]
    
    Eshell V6.4  (abort with ^G)
    1> server_builder:init_amazon().
    ok
    2> server_builder:start_server("webserver"). %% derived from config/webserver_config.cfg